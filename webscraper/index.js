const cheerio = require('cheerio');

(async() => {
    //let browser = await puppeteer.launch();
    var movieUrl = 'https://www.imdb.com/title/tt0848228/?ref_=fn_al_tt_1';
    const options = {
        hostname: 'www.imdb.com',
        port: 443,
        path: '/title/tt0848228/?ref_=fn_al_tt_1',
        method: 'GET'
      };

    let i = 0;
    while(i < 2) {
        //let page = await browser.newPage();
        //await page.goto(movieUrl, {waitUntil: 'networkidle2'});
        await require('https').request(options, (response) =>{
            let $ = cheerio.load(response);
    
            var title = $('div[class="title_wrapper"] > h1').text();
            var ratings = $('span[itemprop="ratingValue"]').text();
            movieUrl = $('div[class="rec_wrapper"] > div[class="rec_const_picker"] > div[class="rec_view"] > div[class="rec_slide"] > div[class="rec_page rec_selected"] > div[class="rec_item"] > a').attr('href');
       
            console.log(title + "            " + ratings);
            // movieUrl changes when i modify it here
            console.log(movieUrl);
        });

        /*let data = await page.evaluate(() => {
            console.log('inside evaluate');
            let title = document.querySelector('div[class="title_wrapper"] > h1').innerText;
            let rating = document.querySelector('span[itemprop="ratingValue"]').innerText;
            //movieUrl = document.querySelector('div[class="rec_overviews"] > div[class="rec_overview"] > div[class="rec_poster"] > a').href;
            // value of movieUrl variable remains unaltered when I modify it here
            movieUrl = document.querySelector('div[class="rec_wrapper"] > div[class="rec_const_picker"] > div[class="rec_view"] > div[class="rec_slide"] > div[class="rec_page rec_selected"] > div[class="rec_item"] > a').href;

            return {
                title,
                rating,
                movieUrl
            }
        });*/

        i++;
    }
    //await browser.close();
})();