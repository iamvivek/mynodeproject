const MongoClient = require('mongodb').MongoClient;

MongoClient.connect('mongodb://localhost:27017', { useNewUrlParser: true }, function(err, client) {
    if(err) {
        console.log('\x1b[91m%s\x1b[0m', 'Failed to connect to MongoDB server.');
        return;
    }

    console.log('\x1b[32m%s\x1b[0m', 'Connected to MongoDB server...');
    // Select the database by name
    const db = client.db('TodoApp');

    // deleteMany
    db.collection('Todos').deleteMany({text: "Buy a goddamn computer"}, (err, result) => {
        if(err) {
            console.log('\x1b[91m%s\x1b[0m', 'Error while deliting, error: ' + err);
            return;
        }
        
        console.log('\x1b[32m%s\x1b[0m', `${result.deletedCount} documents deleted.`);
        console.log(JSON.stringify(result, undefined, 2));
    });

    // deleteOne
    /*db.collection('Todos').deleteOne({text:'Check your emails'}, (err, result) => {
        if(err) {
            console.log('\x1b[91m%s\x1b[0m', 'Error while deliting, error: ' + err);
            return;
        }
        
        console.log('\x1b[32m%s\x1b[0m', `${result.deletedCount} documents deleted.`);
        console.log(JSON.stringify(result, undefined, 2));
    });

    // findOneAndDelete
    db.collection('Todos').findOneAndDelete({text: 'Create a TodoApp'}, (err, result) => {
        if(err) {
            console.log('\x1b[91m%s\x1b[0m', 'Error while deliting, error: ' + err);
            return;
        }
        
        console.log('\x1b[32m%s\x1b[0m', `${result.value} documents deleted.`);
        console.log(JSON.stringify(result, undefined, 2));
    });*/
    client.close();
});