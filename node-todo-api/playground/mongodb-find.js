const MongoClient = require('mongodb').MongoClient;

MongoClient.connect('mongodb://localhost:27017', { useNewUrlParser: true }, function(err, client) {
    if(err) {
        console.log('\x1b[91m%s\x1b[0m', 'Failed to connect to MongoDB server.');
        return;
    }

    console.log('\x1b[32m%s\x1b[0m', 'Connected to MongoDB server...');
    // Select the database by name
    const db = client.db('TodoApp');

    // This one will return all the objects from the database so you might wanna 
    // use cursor instead becuase it will take a lot of memory
    db.collection('Users').find().toArray((err, doc) => {
        if(err) {
            console.log('\x1b[91m%s\x1b[0m', 'Error while processing query database, err: ' + err);
            return;
        }

        console.log(JSON.stringify(doc, undefined, 2));
    });

    client.close();
});