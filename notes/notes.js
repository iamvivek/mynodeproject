const fs = require('fs');

var fetchNotes = () => {
    try {
        var notesString = fs.readFileSync('notes-data.json');
        return JSON.parse(notesString);
    } catch (e) {return [];}
}

var saveNotes = (notes) => {
    fs.writeFileSync('notes-data.json', JSON.stringify(notes));
}

var addNote = (title, body) => {
    if(title === undefined || body === undefined) {
        return "Please specify a title or a body.";
    }

    var notes = fetchNotes();
    var note = {
        title,
        body
    };

    notes.push(note);
    saveNotes(notes);
    return "Note created.\n" + "title: " + note.title + "\nbody: " + note.body;
};

var getAll = () => {
    var notes = fetchNotes();
    
    console.log("Here is all the notes.")
    console.log();

    for(var i = 0; i < notes.length; i++) {
        var note = notes[i];
        console.log("Title: " + note.title);
        console.log("Body: " + note.body);
        console.log();
    }
};

var getNote = (title) => {
    if(title === undefined) return "Please specify a title.";
    var notes = fetchNotes();

    filteredNotes = notes.filter((note) => note.title === title);
    if(filteredNotes.length === 0) {
        console.log("Note with the title " + title + " not found.");
    } else {
        filteredNotes.forEach(element => {
            console.log("Title: " + element.title);
            console.log("Body: " + element.body);
            console.log();
        });
    }
};

var removeNote = (title) => {
    if(title === undefined) return "Please specify a title.";

    var notes = fetchNotes();

    var filteredNotes = notes.filter((note => note.title !== title));
    saveNotes(filteredNotes);
    
    if(filteredNotes.length === notes.length) {
        return "failed to remove note \"" + title + "\".";
    } else {
        return "Note \"" + title +"\" is successfully removed.";
    }
};

module.exports = {
    addNote,
    getAll,
    getNote,
    removeNote
};