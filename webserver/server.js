const express = require('express');
const hbs = require('hbs');

const port = process.env.PORT || 3000;
var app = express();

hbs.registerPartials(__dirname + "/views/partials");
app.set('view engine', 'hbs');
app.use(express.static(__dirname + '/static'));

/*app.use((req, res, next) => {
    res.render('maintainence.hbs');
});*/


app.get("/", (request, response) => {
    response.render('home.hbs', {
        pageTitle:"Home Page",
        welcomeMessage:"You are looking at home page.",
        currentYear:new Date().getFullYear()
    })
});

app.get('/about', (req, res) => {
    res.render('about.hbs', {
        pageTitle:"About Page",
        welcomeMessage:"You are looking at about page.",
        currentYear:new Date().getFullYear()
    });
});

app.get('/projects', (req, res) => {
    res.render('projects.hbs', {
        pageTitle:"Projects Page",
        welcomeMessage:"you are looking at projects page.",
        currentYear:new Date().getFullYear()
    })
});

app.get('/bad', (req, res) => {
    res.send({
        status:404,
        errorMsg:'Looks like you\'re lost' 
    });
});

app.listen(port, () => {
    console.log('\x1b[36m%s\x1b[0m', `Server is up on port ${port}.`);
});